<?php
/**
 * @author   	cg@089webdesign.de
 */
 defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-De">
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS ?>		
		<?php // <link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/089-template/fonts/dosis-regular-webfont.ttf"> ?>
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/089-template/fonts/assistant-regular-webfont.ttf">
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass; ?>">

	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<?php			
			
			// including Menu
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
			?>
			<div id="aboveTHeFold">
			<?php
				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
				
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
										
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			?>
			</div>
			<div id="belowTheFold">
			<?php 						
				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');				
				
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				
				// including bottom2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
				
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');								
			?>					
			</div>
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />
	<?php /* GOLive: wird noch gebraucht?-> <script src="<?php print '/templates/' . $this->template . '/js/script.js';?>" type="text/javascript"></script> */ ?>

	<script type="text/javascript">

		jQuery(document).ready(function() {

			jQuery('.breadcrumb li:nth-child(3) .pathway').on('click', function(e){ <?php //CG: parent-Menues nicht klickbar machen im breadcrumb ?>
				e.preventDefault();
			}); <?php //CG: aber nicht für die Unterpunkte von Datenschutz: ?>
				if(jQuery('body').hasClass('itemid-210') || jQuery('body').hasClass('itemid-211') || jQuery('body').hasClass('itemid-212') || jQuery('body').hasClass('itemid-213')) {
					disclaimerPath = jQuery('.breadcrumb li:nth-child(3) .pathway').attr('href');
					jQuery('.breadcrumb li:nth-child(3) .pathway').click("on", function(){
						window.location = disclaimerPath;
					});
				}

			<?php if ($detect->isMobile()) : ?> //mobile

				<?php //mobile menu open/close ?>
				jQuery('.btn-navbar').click("on", function() {
					jQuery('.nav-collapse.collapse').toggleClass('openMenu');
					jQuery('button.btn-navbar').toggleClass('btn-modify');
				});
				jQuery('li.dropdown.parent a').click(function(){
					jQuery(this).next('.dropdown-menu').slideToggle();

				});
			<?php else: ?>
				jQuery(window).scroll(function () { <?php //CG: sticky ?>
					(jQuery(this).scrollTop() > 125) ? jQuery('#menuWrapper').addClass('sticky') : jQuery('#menuWrapper').removeClass('sticky');
				});

				<?php if($is_home) : // desktop startseite scroll fadeIn/out: ?>
					jQuery(window).scroll(function() {
					    var windowBottom = jQuery(this).scrollTop() + jQuery(this).innerHeight();
					    jQuery(".startFade").each(function() {
					      var objectBottom = (jQuery(this).offset().top + jQuery(this).outerHeight()) - 100;				      
					      if (objectBottom < windowBottom) { 
					      	if (jQuery(this).css("opacity")==0) {jQuery(this).fadeTo(800,1);}
					      	} else { 
					        if (jQuery(this).css("opacity")==1) {jQuery(this).fadeTo(800,0);}
					      }
					    });
					  }).scroll();
				<?php endif; ?>
			<?php endif;  ?>
			<?php if($is_home && $detectAgent != "phone ") : ?>
				jQuery(window).scroll(function () { <?php //CG: auch für mobile?? ?>
				    //parallax bckgrImg:
				    <?php if($detectAgent == "desktop ") : ?>
				    	jQuery(".top.navCellHome").css("background-position","0% " + (-700 + (jQuery(this).scrollTop() / 1.7)) + "px");					
				    <?php elseif($detectAgent == "tablet ") : ?>
				    	jQuery(".top.navCellHome").css("background-position","0% " + (-400 + (jQuery(this).scrollTop() / 2.7)) + "px");
				    <?php endif; ?>
				    // CG: add css-class falls scrolled @ menuwrapper

				    var scrollPercent = (500 - window.scrollY) / 500;
				    var topScroll = ((jQuery(this).scrollTop() / 1.7)) + "px";

					    jQuery(".homePresent").css({top: topScroll, opacity: scrollPercent});
					    //console.log(scrollPercent);

				});
			<?php endif;?>
		});

	</script>
	<?php if(!$clientMobile) : ?>
		<div id="resizeAlarm">
			<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergr&ouml;ssern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
		</div>
	<?php endif; ?>	
</body>
</html>
