<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer startFade" role="contentinfo">

	<?php if($seoPage) : ?>
			<jdoc:include type="modules" name="seoFooter" style="custom" />
	<?php else:  ?>
		<div class="footer-wrap">											
			<div class="vcard clr">
				<img class="logo" src="/images/kanzlei-hetsch-logo-small.jpg" alt="Steuerkanzlei Hetsch Firmenlogo">			  
				<p class="org ">Steuerkanzlei Hetsch</p>
				<p class="adr"><span class="street-address">Untere Bahnhofstraße 38 A </span><br />
					<span class="postal-code">82110 </span><span class="region">Germering</span>
				</p>
				<p class="tel "><a class="" href="tel:+8999829280">Tel: 089-99 82 928-0</a></p>
			</div>
			<?php /* 			
			<ul class="socialMedia">
				<li>
					<a href="/">
						<img src="/images/facebook-icon-kanzlei-hetsch.png" alt="Kanzlei Hetsch bei Facebook" />
					</a>
				</li>
				<li>
					<a href="/">
						<img src="/images/twitter-icon-kanzlei-hetsch.png" alt="Kanzlei Hetsch bei Twitter" />
					</a>
				</li>			
			</ul>
			*/ ?>
		</div>
	<?php endif; ?>
	<div id="copyright" class="fullwidth">
		<div class="copyWrapper innerwidth">
			<p>&copy; <?php print date("Y") . ' ' . $sitename; ?> | <a class="imprLink" href="/impressum.html">Impressum</a> | <a class="imprLink" href="/datenschutz.html">Datenschutzerkl&auml;rung</a></p>
		</div>
	</div>
	<?php if($this->countModules('landingpage-link')) : ?>
		<div class="seopage-link">
			<jdoc:include type="modules" name="landingpage-link" style="none" />
		</div> 
	<?php endif;?>		
</footer>
		