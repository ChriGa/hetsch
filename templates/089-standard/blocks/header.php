<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
	
<header id="header" class="fullwidth">
	<div class="vcard clr">			  
		<p class="org ">Steuerkanzlei Hetsch</p>
		<p class="adr"><span class="street-address">Untere Bahnhofstraße 38 A </span><br />
			<span class="postal-code">82110 </span><span class="region">Germering</span>
		</p>
		<p class="tel "><a class="" href="tel:+8999829280">Tel: 089-99 82 928-0</a></p>
	</div>         
</header>