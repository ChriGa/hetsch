<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


defined('_JEXEC') or die;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/jquery.lazyloadxt.extra.min.js');


// Add Stylesheets
$doc->addStyleSheet('templates/' . $this->template . '/css/normalize.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/responsive.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', true, $this->direction);

// Add Stylesheets - CG overrides als letztes!!
$doc->addStyleSheet('templates/' . $this->template . '/css/overrides.css');

?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />

	<link rel="canonical" href="<?php print JURI::current(); ?>" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->

<?php // CG: bugherd -script nach Go-Live entfernen: ?>
<script type='text/javascript'>
	(function (d, t) {
	  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
	  bh.type = 'text/javascript';
	  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=sosgt0gl2er6jmrf2xvjqg';
	  s.parentNode.insertBefore(bh, s);
	  })(document, 'script');
</script>	