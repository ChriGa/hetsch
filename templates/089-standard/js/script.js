/*
 * 0911 JS scripts 
 */
// bckgr Image parallax scroll

jQuery(window).scroll(function () {
    //parallax bckgrImg:
    jQuery(".top.navCellHome").css("background-position","0% " + (-700 + (jQuery(this).scrollTop() / 1.7)) + "px");
    // add css-class if scrolled @ menuwrapper
    (jQuery(this).scrollTop() > 125) ? jQuery('#menuWrapper').addClass('sticky') : jQuery('#menuWrapper').removeClass('sticky');
});